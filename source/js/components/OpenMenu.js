class OpenMenu {
  constructor(initParams) {
    const defaultParams = {
      headerSelector: '.js-header-container',

      buttonOpenMenuSelector: '.js-mobile-button',
      buttonOpenClass: 'mobile-button_state_open',

      menuSelector: '.js-header-menu',
      menuOpenClass: 'header__menu_state_open',

      desktopPoint: '1240',
    }

    const params = {...defaultParams, ...initParams}

    const menu = document.querySelector(params.menuSelector)
    const header = document.querySelector(params.headerSelector)

    const buttonOpen = document.querySelector(params.buttonOpenMenuSelector)

    if (!menu || !buttonOpen || !header) throw new Error('Please, check menu selectors.')

    this.header = header
    this.desktopPoint = params.desktopPoint

    this.menu = menu
    this.menuOpenClass = params.menuOpenClass
    this.menuIsShow = false

    this.buttonOpen = buttonOpen
    this.buttonOpenClass = params.buttonOpenClass

    this.onOpenButtonClick = () => {
      if (this.menuIsShow) {
        this.setMenuClose()
      } else {
        this.setMenuOpen()
      }
    }

    this.onWindowResize = this.debounce(() => {
      if (this.isDesktop()) {
        this.setMenuClose()
      }
    }, 250)
  }

  init() {
    this.buttonOpen.addEventListener('click', this.onOpenButtonClick)
    window.addEventListener('resize', this.onWindowResize)
  }

  setMenuOpen() {
    this.menuIsShow = true
    this.menu.classList.add(this.menuOpenClass)
    this.menu.style.height = `calc(100vh - ${this.header.clientHeight}px`
    this.buttonOpen.classList.add(this.buttonOpenClass)
  }

  setMenuClose() {
    this.menuIsShow = false
    this.menu.classList.remove(this.menuOpenClass)
    this.menu.style.height = ''
    this.buttonOpen.classList.remove(this.buttonOpenClass)
  }

  isDesktop() {
    return window.matchMedia(`(min-width: ${this.desktopPoint}px)`).matches
  }

  debounce(func, wait, immediate) {
    let timeout;

    return function executedFunction() {
      const context = this
      const args = arguments;

      const later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };

      const callNow = immediate && !timeout;

      clearTimeout(timeout);

      timeout = setTimeout(later, wait);

      if (callNow) func.apply(context, args);
    }
  }
}

window.OpenMenu = OpenMenu
