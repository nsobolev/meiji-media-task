// Для добавления функционала используем классы с префиксом js, стилизовать по этим классам нельзя

$(document).ready(function(){
  const menu = new window.OpenMenu()

  if (menu) {
    menu.init()
  }
});
