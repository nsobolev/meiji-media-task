'use strict';

const gulp = require('gulp')
const svgSprite = require('gulp-svg-sprite')
const svgmin = require('gulp-svgmin')
const replace = require('gulp-replace')

module.exports = require('bootstrap-gulp');

gulp.task('sprite', function () {
  return gulp.src('source/svg/*.svg')
    .pipe(svgmin({
      js2svg: {
        pretty: true
      }
    }))
    .pipe(replace('&gt;', '>'))
    .pipe(svgSprite({
      mode: {
        symbol: {
          sprite: '../sprite.svg',
        }
      }
    }))
    .pipe(gulp.dest('dist/svg'))
})
